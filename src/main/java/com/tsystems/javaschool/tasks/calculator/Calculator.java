package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Locale;

public class Calculator {
    private static HashMap<String, Integer> operations = new HashMap<>();

    static {
        operations.put("+", 1);
        operations.put("-", 1);
        operations.put("*", 2);
        operations.put("/", 2);
    }

    /**
     * Compares two operations
     * @param firstOperation
     * @param secondOperation
     * @return true if the first operations has a higher or same priority
     */
    private Boolean compareOperations(String firstOperation, String secondOperation) {
        return operations.containsKey(firstOperation)
                && (operations.get(firstOperation) >= operations.get(secondOperation));
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        Double result;
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
        otherSymbols.setDecimalSeparator('.');
        DecimalFormat formatter = new DecimalFormat("#.####", otherSymbols);
        try {
            result = rpn(convertToPrefix(statement));
        } catch (Exception e) {
            return null;
        }

        return formatter.format(result);
    }

    /**
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return queue that is a reverse polish notation
     */
    private ArrayDeque<String> convertToPrefix(String statement) {
        ArrayDeque<String> operationStack = new ArrayDeque<>();
        ArrayDeque<String> outputQueue = new ArrayDeque<>();

        for (String element : statement.split("(?=[-+*/()])|(?<=[-+*/()])")) {
            if (operations.containsKey(element)) {
                while (!operationStack.isEmpty() && compareOperations(operationStack.peek(), element)) {
                    outputQueue.offer(operationStack.pop());
                }
                operationStack.push(element);
            } else if (element.equals("(")) {
                operationStack.push(element);
            } else if (element.equals(")")) {
                while (!operationStack.peek().equals("(")) {
                    outputQueue.offer(operationStack.pop());
                }
                operationStack.pop();
            } else {
                outputQueue.offer(element);
            }
        }

        while (!operationStack.isEmpty()) {
            outputQueue.offer(operationStack.pop());
        }

        return outputQueue;
    }

    /**
     * @param rpnQueue result of convertToPrefix
     * @return result of calculation
     * @throws Exception
     */
    private Double rpn(ArrayDeque<String> rpnQueue) throws Exception {
        ArrayDeque<Double> stack = new ArrayDeque<>();

        while (!rpnQueue.isEmpty()) {
            String element = rpnQueue.poll();
            switch (element) {
                case "+":
                    stack.push(stack.pop() + stack.pop());
                    break;
                case "-":
                    stack.push(-stack.pop() + stack.pop());
                    break;
                case "*":
                    stack.push(stack.pop() * stack.pop());
                    break;
                case "/":
                    double divisor = stack.pop();
                    if (divisor == 0) throw new Exception("Division by zero!");
                    stack.push(stack.pop() / divisor);
                    break;
                default:
                    stack.push(Double.valueOf(element));
            }
        }
        return stack.pop();
    }
}
