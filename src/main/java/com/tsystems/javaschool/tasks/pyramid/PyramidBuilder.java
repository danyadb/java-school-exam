package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null) || inputNumbers.size() == 0) throw new CannotBuildPyramidException();

        int baseY = checkListSize(inputNumbers.size());
        int baseX = baseY + baseY - 1;
        Collections.sort(inputNumbers);
        ListIterator<Integer> it = inputNumbers.listIterator();
        int start = baseX / 2;
        int[][] pyramid = new int[baseY][baseX];

        for (int row = 0; row < baseY; ++row, --start) {
            for (int numsInRow = 0; numsInRow < row + 1; ++numsInRow) {
                pyramid[row][start + numsInRow * 2] = it.next();
            }
        }

        return pyramid;
    }

    /**
     * @param size size of inputNumbers
     * @return height of pyramid
     * @throws CannotBuildPyramidException
     */
    private Integer checkListSize(Integer size) {
        int base = (int) Math.round(Math.sqrt(size * 2));

        if ((base * (base + 1) / 2 != size)) {
            throw new CannotBuildPyramidException();
        }
        return base ;
    }
}
