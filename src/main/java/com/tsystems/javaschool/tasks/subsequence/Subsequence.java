package com.tsystems.javaschool.tasks.subsequence;

import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Subsequence {
    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) throw new IllegalArgumentException();
        if (x.size() > y.size()) return false;
        else if (x.isEmpty()) return true;

        ListIterator itX = x.listIterator();

        for (Object o : y) {
            if (!itX.hasNext()) {
                return true;
            } else if (!itX.next().equals(o)) {
                itX.previous();
            }
        }

        return false;
    }
}

